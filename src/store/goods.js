import request from '../utils/request'
import { fetchGoodsList, addGoods, updateGoods, deleteGoods } from '../api/goods'

export default {
  namespaced: true,
  state: {
    list: [],
    total: 0
  },
  getters: {

  },
  mutations: {
    fetchGoodsSuccess (state, payload) {
      const { result } = payload
      result && Object.keys(result).forEach(key => {
        state[key] = result[key]
      })
    },
    addItemSuccess (state, payload) {
    },
    updateGoodsSuccess (state, payload) {
    },
    deleteGoodsSuccess (state, payload) {
    }
  },
  actions: {
    async fetchGoods ({commit}, data) {
      try {
        const res = await fetchGoodsList(data)
        commit({type: 'fetchGoodsSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
    async addItem ({commit}, data) {
      try {
        const res = await addGoods(data)
        commit({type: 'addItemSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
    async update ({commit}, data) {
      try {
        const res = await updateGoods(data)
        commit({type: 'updateGoodsSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
    async delete ({commit}, data) {
      try {
        const res = await deleteGoods(data)
        commit({type: 'deleteGoodsSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    }
  }
}
