import request from '@/utils/request'

const path = '/admin/1.0/auth/biz/article'

/**
 * 获取商品列表
 */

export const fetchArticleList = data => request(path, {data: {s: 20, ...data}, method: 'GET'});

/**
 * 添加商品
 */
export const addArticle = data => request(path, {data, method: 'POST'})

/**
 * 修改商品信息
 * @param {} data 
 */
export const updateArticle = data => {
  const {article} = data
  const {id} = article || {}
  request(path + '/' + id, {data, method: 'PUT'})
}

/**
 * 删除商品
 * @param {*} param0 
 */
export const deleteArticle = (data) => {
  const { id } = data
  delete data.id
  request(path + '/' + id, {data, method: 'DELETE'})
}