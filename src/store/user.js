import request from '../utils/request'
import { fetchUsers, fetchUserDetail } from '../api/user'

export default {
  namespaced: true,
  state: {
    list: [],
    total: 0,
    item: [],
    itemTotal: 0
  },
  getters: {

  },
  mutations: {
    fetchUsersSuccess (state, payload) {
      const { result } = payload
      result && Object.keys(result).forEach(key => {
        state[key] = result[key]
      })
    },
    fetchItemSuccess (state, payload) {
      const { result } = payload
      state.item = result ? result.outFits : []
    },
  },
  actions: {
    async fetchUserList ({commit}, data) {
      try {
        const res = await fetchUsers(data)
        commit({type: 'fetchUsersSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
    async fetchItem ({commit}, data) {
      try {
        const res = await fetchUserDetail(data)
        commit({type: 'fetchItemSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
  }
}
