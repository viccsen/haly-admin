import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
import auth from './auth'
import article from './article'
import goods from './goods'
import user from './user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    router,
    auth,
    article,
    goods,
    user,
  }
})

export default store
