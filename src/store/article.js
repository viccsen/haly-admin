import request from '../utils/request'
import { fetchArticleList, addArticle, updateArticle, deleteArticle } from '../api/article'

export default {
  namespaced: true,
  state: {
    list: [],
    total: 0
  },
  getters: {

  },
  mutations: {
    fetchGoodsSuccess (state, payload) {
      const { result } = payload
      result && Object.keys(result).forEach(key => {
        state[key] = result[key]
      })
    },
    addItemSuccess (state, payload) {
    },
    updateGoodsSuccess (state, payload) {
      const { result } = payload
      if(result){
        state.list.forEach(it => {
          if(it.id == result.id){
            it = {...result}
          }
        })
      }
    },
    deleteGoodsSuccess (state, payload) {
    }
  },
  actions: {
    async fetchArticle ({commit}, data) {
      try {
        const res = await fetchArticleList(data)
        commit({type: 'fetchGoodsSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
    async addItem ({commit}, data) {
      try {
        const res = await addArticle(data)
        commit({type: 'addItemSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
    async update ({commit}, data) {
      try {
        const res = await updateArticle(data)
        commit({type: 'updateGoodsSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
    async delete ({commit}, data) {
      try {
        const res = await deleteArticle(data)
        commit({type: 'deleteGoodsSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    }
  }
}
