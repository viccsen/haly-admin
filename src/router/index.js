import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import AuthCache from '../cache/auth'

Vue.use(Router)

const Login = r => require.ensure([], () => r(require('@/page/login')), 'login');
const Manage = r => require.ensure([], () => r(require('@/page/manage')), 'manage');
const Home = r => require.ensure([], () => r(require('@/page/home')), 'home');
const Articles = r => require.ensure([], () => r(require('@/page/articles')), 'articles');
const Goods = r => require.ensure([], () => r(require('@/page/goods')), 'goods');
const Users = r => require.ensure([], () => r(require('@/page/users')), 'users');
const UserDetail = r => require.ensure([], () => r(require('@/page/userDetail')), 'userDetail');

const router = new Router({
	strict: process.env.NODE_ENV !== 'production',
	routes: [
		{
			path: '/login',
			component: Login
		},
		{
			path: '/',
			component: Manage,
			name: 'home',
			children: [{
				path: '',
				component: Home,
				meta: [],
			},{
				path: '/articles',
				name: 'articles',
				component: Articles,
				meta: ['数据管理', '资讯列表'],
			},{
				path: '/goods',
				name: 'goods',
				component: Goods,
				meta: ['内容管理', '商品列表'],
			},{
				path: '/users',
				name: 'users',
				component: Users,
				meta: ['用户管理', '用户列表'],
			},{
				path: '/user-detail',
				name: 'userDetail',
				component: UserDetail,
				meta: ['用户管理', '用户详情']
			}]
		},
		{  
      path: '*',  
      redirect: '/'  
    }
	]
})

let updateLoadingStatus_sid

router.beforeEach(function (to, from, next) {
  next()

})

router.afterEach(function (to) {
  const auth = AuthCache()
	const {token} = auth || {};
	const {meta} = to
  if(to.path !== '/login' && !token){
	  router.push('/login')
	}
	if(meta[1] || meta[0]){
		document.title = meta[1] || meta[0]
	}
})

export default router
