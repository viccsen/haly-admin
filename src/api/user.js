import request from '@/utils/request'

const path = '/admin/api/business/user'

/**
 * 获取用户列表
 */

export const fetchUsers = data => request(path, {data: {s: 20, ...data}, method: 'GET'});

/**
 * 获取用户信息
 */

export const fetchUserDetail = data => {
    const { id } = data
    delete data.id
    return request(path + '/' + id, { method: 'GET' });
}
