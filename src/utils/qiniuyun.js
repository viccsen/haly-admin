export default class Qiniuyun {
  constructor (options = {}) {
    this._options = {...options}
  }

  ajax (url = '', opt = {}) {
    const options = {method: 'GET', async: true, dataType: 'JSON', ...opt}
    return new Promise((resolve, reject) => {
      const ajax = this.createAjax()
      if (ajax) {
        const _async = typeof options.async === 'boolean' ? options.async : true
        ajax.open(options.method || 'GET', url, _async)
        if (options.headers) {
          Object.keys(options.headers).forEach(key => {
            ajax.setRequestHeader(key, options.headers[key])
          })
        }
        ajax.onreadystatechange = () => {
          if (ajax.readyState === 4) {
            if (ajax.status >= 200 && ajax.status <= 400) {
              let res = ajax.responseText
              if (options.dataType && options.dataType.toUpperCase() === 'JSON') {
                res = JSON.parse(res)
              }
              resolve(res, ajax.response)
            } else {
              reject(new Error('请求失败:' + ajax.status), ajax.status)
            }
          }
        }
        ajax.send(options.data)
      } else {
        reject(new Error('创建Ajax请求失败！'))
      }
    })
  }

  createAjax () {
    let xmlhttp
    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest()
    } else {
      xmlhttp = new ActiveXObject('Microsoft.XMLHTTP')
    }
    return xmlhttp
  }

  getToken () {
    return this.ajax(this._options.getTokenUrl, {
      headers: {
        ...this._options.getTokenHeaders,
      }
    }).then((res) => {
      console.log('token', res.result)
      return res.result
    }).catch(ex => {
      throw new Error('获取uptoken失败:' + ex.message)
    })
  }

  upload (file, filename, key) {
    return this.getToken().then(token => {
      const formData = new FormData()
      formData.append('key', key || filename)
      formData.append('token', token)
      formData.append('file', file, filename)
      return formData
    }).then(data => {
      return this.ajax('http://upload.qiniu.com/', {
        method: 'POST',
        data
      }).then(res => {
        res.url = this._options.domain + res.key
        res.thum = res.url + '!t'
        return res
      })
    })
  }
}

