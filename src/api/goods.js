import request from '@/utils/request'

const path = '/admin/api/business/item'

/**
 * 获取商品列表
 */

export const fetchGoodsList = data => request(path, {data: {s: 20, ...data}, method: 'GET'});

/**
 * 添加商品
 */
export const addGoods = data => request(path, {data, method: 'POST'})

/**
 * 修改商品信息
 * @param {} data 
 */
export const updateGoods = data => {
  const {item} = data
  const {id} = item || {}
  request(path + '/' + id, {data, method: 'PUT'})
}

/**
 * 删除商品
 * @param {*} param0 
 */
export const deleteGoods = (data) => {
  const {id} = data
  delete data.id
  request(path + '/' + id, {data, method: 'DELETE'})
}