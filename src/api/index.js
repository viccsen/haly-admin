import request from '@/utils/request'
import authenticateCache from '../cache/auth';


/**
 * 七牛token
 */

export const fetchQiniuToken = data =>  request('/admin/1.0/auth/biz/qiniu/token', {data, method: 'GET'})

/**
 * 登陆
 */

export const login = data => request('/admin/api/authenticate', {data, method: 'POST'}, authenticateCache)
